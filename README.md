# ConcurrentLinkedHashMap

Built as an experiment since I couldn't find a satisfactory data structure in the Java core libs, mainly, I'm assuming
because the use cases for something like this are so limited. If you wish to use this, I do not have a maven repository
set up, so simply clone and run `gradle publishToMavenLocal`. And then in your `build.gradle`
``` gradle
dependencies {
    …
    compile "com.gitlab.demack:concurrent-linked-hash-map:1.0-SNAPSHOT"
    …
}
```

__NOTE:__ The only testing that has been done on this are the tests included in this repository. Feel free to use this if
it fits your needs, but I can't say what will and won't work.

