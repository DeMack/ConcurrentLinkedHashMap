package com.gitlab.demack.concurrentlinkedhashmap

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue

class ConcurrentLinkedHashMap<K, V>(internal val insertionOrder: ConcurrentLinkedQueue<K> = ConcurrentLinkedQueue())
    : ConcurrentHashMap<K, V>(), Iterable<Pair<K, V>> {

    var first: V?
        get() = when {
            insertionOrder?.isNotEmpty() -> this[insertionOrder.first()]
            else -> null
        }
        private set(value) {
            /*Purposefully left blank*/
        }
    var last: V?
        get() = when {
            insertionOrder?.isNotEmpty() -> this[insertionOrder.last()]
            else -> null
        }
        private set(value) {
            /*Purposefully left blank*/
        }

    override fun put(key: K, value: V): V? {
        insertionOrder.add(key)
        return super.put(key, value)
    }

    override fun remove(key: K): V? {
        insertionOrder.remove(key)
        return super.remove(key)
    }

    override fun iterator() = LinkedConcurrentHashMapIterator(this)

}

class LinkedConcurrentHashMapIterator<K, V>(private val linkedConcurrentHashMap: ConcurrentLinkedHashMap<K, V>) : Iterator<Pair<K, V>> {
    private val traverser = linkedConcurrentHashMap.insertionOrder.iterator()

    override fun hasNext() = traverser.hasNext()
    override fun next(): Pair<K, V> {
        var key = traverser.next()
        return Pair(key!!, linkedConcurrentHashMap[key]!!)
    }
}
