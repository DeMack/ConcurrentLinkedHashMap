package com.gitlab.demack.concurrentlinkedhashmap

import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ConcurrentLinkedHashMapTest {
    private val kvs = arrayOf("KEY1", "VAL1", "KEY2", "VAL2", "KEY3", "VAL3", "KEY4", "VAL4", "KEY5", "VAL5")

    private val map = ConcurrentLinkedHashMap<String, String>()

    @Before
    fun setup() {
        for (i in 0 until kvs.size - 1 step 2) {
            map.put(kvs[i], kvs[i + 1])
        }
    }

    @Test
    fun first() {
        assertEquals("First value should be VAL1", "VAL1", map.first)
    }

    @Test
    fun last() {
        assertEquals("Last value should be VAL5", "VAL5", map.last)
    }

    @Test
    fun put() {
        assertEquals("map should contain 5 items", 5, map.size)
    }

    @Test
    fun remove() {
        map.remove("KEY3")
        assertEquals("map should contain 4 items", 4, map.size)
        assertFalse("KEY3 should have been removed from map", map.contains("KEY3"))
    }

    @Test
    fun getInsertionOrder() {
        assertEquals("insertionOrder should contain 5 items", 5, map.insertionOrder.size)
        var i = 1
        var iterator = map.insertionOrder.iterator()
        while (iterator.hasNext()) {
            assertEquals("""map.insertionOrder[$i] should be KEY$i""", """KEY$i""", iterator.next())
            i++
        }
    }

    @Test
    operator fun iterator() {
        var i = 1
        for ((key, value) in map) {
            assertEquals("""key should be KEY$i""", """KEY$i""", key)
            assertEquals("""map.[$i] should be VAL$i""", """VAL$i""", value)
            i++
        }
    }

    @Test
    fun givenEmptyMap_whenFirstOrLastCalled_thenExpectNullValues() {
        val mtMap: ConcurrentLinkedHashMap<Int, Int> = ConcurrentLinkedHashMap()
        assertNull(mtMap.first)
        assertNull(mtMap.last)
    }

    @Test
    fun givenEmptyMap_whenPopulatedOnMultipleThreads_thenExpectProperOrderPreserved() {
        val linkedConcurrentHashMap = ConcurrentLinkedHashMap<String, String>()

        runBlocking {
            for (i in 0..1_000_000) {
                linkedConcurrentHashMap.put("""KEY$i""", """VAL$i""")
            }
        }

        for ((i, pair) in linkedConcurrentHashMap.withIndex()) {
            var (key, value) = pair
            assertEquals("""key should be KEY$i""", """KEY$i""", key)
            assertEquals("""map.[$i] should be VAL$i""", """VAL$i""", value)
        }
    }
}